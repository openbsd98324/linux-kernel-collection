# linux-kernel-collection


## debian Kmuto


## 2010 Debian, Squeeze

Index of /debian/pool/main/l/linux-2.6http://archive.debian.org › debian › main › linux-2
Index of /debian/pool/main/l/linux-2.6 ... linux-headers-2.6.32-5-686-bigmem_2.6.32-48squeeze6_i386.deb, 2014-05-14 17:28, 533K.


## Raspberry Pi

linux-debian-bookworm-sid-5.14-kernel-lib-modules-boot-arm64-aka-aarch64-v2.tar.gz

## Linux Kernel Stretch (Pi First, gen. rpi3b)

https://gitlab.com/openbsd98324/linux-kernel-collection/-/raw/main/1651568499-1-Linux-kernel-raspbian-stretch-4.9.41-v7+-classic-2017-kernel-modules-sd-v1-piw-bogo-38-40-armv7-proc-rev-4-v7l-1.tar.gz

## Working 

````
qemu-system-aarch64   -M versatilepb    -cpu arm1176  -m 256  -kernel /media/sda1/vmlinuz-4.14.0-3-arm64    -initrd /media/sda1/initrd.img-4.14.0-3-arm64  -dtb /media/sda1/bcm2837-rpi-3-b.dtb    (...)
````


## Notes

devuan ascii amd64 : kernel (na.)

devuan bookworm amd64 : kernel (na.)

devuan chimaera amd64 : kernel (na.)



## Archive

````
qemu-system-aarch64 -M raspi3  -serial mon:stdio \
-kernel vmlinuz-4.14.0-3-arm64 \
-initrd initrd.img-4.14.0-3-arm64 \
-dtb bcm2837-rpi-3-b.dtb \
-append "rw earlycon=pl011,0x3f201000 console=ttyAMA0 loglevel=8 root=/dev/mmcblk0p2 fsck.repair=yes net.ifnames=0 rootwait memtest=1" \
-drive file=2018-01-08-raspberry-pi-3-buster-PREVIEW.img,format=raw,if=sd \
-device usb-net
````



## AMD64


1.) devuan 
4.9.x

 
2.) bookworm, Debian SID  and bookworm
see especially https://gitlab.com/openbsd98324/linux-debian-bookworm/ for 
1642356519-1-linux-kernel-boot-vmlinuz-5.15.0-2-amd64-bookworm-v1.tar.gz 

## i386

devuan

https://gitlab.com/openbsd98324/linux-kernel-collection/-/raw/main/1647082259-1-linux-devuan-fromlive-ascii-i386-4.9.0-11-686-kernel-modules-v1.2.tar.gz


## efi ubuntu + opensuse leap version 1.1.

(N.A.)

## efi win dual, ubuntu with 5.x sig version 1.2

https://gitlab.com/openbsd98324/dell-precision-3561/-/raw/main/pub/linux/ubuntu/linux-ubuntu-kernel-modules-sig-uefi-nvm-5.19.0-26-generic-precision-v1.1-1671245366.tar.gz



## Pinebook Pro

firmware and modules

https://gitlab.com/openbsd98324/linux-kernel-collection/-/raw/main/lib-pinebook-pro-modules+firmware-only-202301-manjaro-5.18.0-4-MANJARO-ARM-min-modules-firmware-kdedesk-mobile-v1.2.tar.gz


## References 

https://people.debian.org/~stapelberg/raspberrypi3/2018-01-08/2018-01-08-raspberry-pi-3-buster-PREVIEW.img.xz


